# Mensch ärgere Dich nicht

German board game 'Mensch ärgere Dich nicht' for the Casio ClassPad II.

The following variant of the game is used:
- Three tries to get a six in the beginning. This also applies when all your figures are either out or at the end of the home row. 
- No overtaking in the home row
- No obligation to throw other players figures out

Controls: Touch. Press [ ] on the right to roll the dice.

The xcp-file is the compressed program with comments removed. Install: Copy via USB, then use System->Storage/Import

Disclaimer: Too many variables and programs can crash the calculator. Backup important data and watch out in exams.

Have fun!
